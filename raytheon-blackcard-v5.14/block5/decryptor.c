#include <ctype.h>
#include <string.h>
#include <stdio.h>


void enc(char* flag, unsigned char key)
{
	while(*flag)
	{	
		*flag ^= (key=(key*13)+37);
		*(flag++) += 3;
	}
}
/* Output: 3C F7 BF 3C D9 53 49 57 33 27 68 BA 70 28 65 */

void dec(char* flag, unsigned char key)
{
	while(*flag)
	{
		*flag -= 3;
		*flag ^= (key=(key*13)+37);
		flag++;
	}
}

int main()
{
	//const unsigned char* ciphertext = "\x3C\xF7\xBF\x3C\xD9\x53\x49\x57\x33\x27\x68\xBA\x70\x28\x65";
	//int TEXT_LEN = 15;
	const unsigned char* ciphertext = "\x0F\xA8\x9F\xFE\x7A\xD6\xE2\x08\xAE\x2B\x5F\x53\x25\x9A";
	int TEXT_LEN = 14;
	
	char unsigned flag[TEXT_LEN];
	char unsigned output[16];
	
	unsigned int key;
	for (key=0; key < 256; key++)
	{
		memset(flag, '\0', TEXT_LEN);
		memcpy(flag, ciphertext, TEXT_LEN);
		dec(flag, key);
		//printhex(flag, "plaintext ", TEXT_LEN);
	
		//brute force print. Pipe to file and run strings	
		/*memset(output, '\0', TEXT_LEN + 1);
		memcpy(output, flag, TEXT_LEN);
		printf("%d:%s\n", key, output);
		*/


		//It's probably a printable string...
		if (1 == fullyPrintable(flag))
			printf("%d: %s\n", key, flag);
	}
	
	return 0;
}


void printhex(const unsigned char* data, const char* prefix, int TEXT_LEN)
{
	printf("%s: ", prefix);
	int i;
	for(i=0; i < TEXT_LEN; i++)
	{
		//printf("%03d ", data[i]);
		printf("0x%02X ", data[i]);
	}
	printf("\n");	
}

int fullyPrintable(const char* data)
{
	while(*data)
	{
		if (! isprint(*data))
			return -1;
		data++;
	}
	return 1;
}


