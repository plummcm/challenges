import sys
import binascii

def keygen(numbytes):
    key = [-1,-1,-1,-1]
    for i in range(numbytes):
        for a in range(256):
            key[0] = a
            for b in range(256):
                key[1] = b
                for c in range(256):
                    key[2] = c
                    for d in range(256):
                        key[3] = d
                        yield key

def ksa(key, keylength):
    s = []
    for i in range(256):
        s.append(i)

    j = 0
    for i in range(256):
        
        j = (j + s[i] + key[i % keylength]) % 256
        
        #swap s[i] w/ s[j]
        tmp = s[i]
        s[i] = s[j]
        s[j] = tmp
    
    return s

def crypt(data, s):
    i = 0
    j = 0

    k = []

    for x in data:
        i = (i + 1) % 256
        j = (j + s[i]) % 256

        tmp = s[i]
        s[i] = s[j]
        s[j] = tmp

        k.append( s[ (s[i] + s[j]) % 256 ] )

    return k

def niceprint(data):
    for d in data:
        h = binascii.unhexlify('%02x' % d)
        sys.stdout.write(h)

    sys.stdout.flush()
    print ""



#main
ciphertext_v514 = [0xF4, 0x73, 0xF1, 0x30, 0x24, 0xB5, 0x4F, 0x35, 0x58, 0xF1, 0x62, 0x48, 0x55]
numbytes = 4
while(keygen(numbytes)):
    for brute in keygen(numbytes):
        newkey = ksa(brute, numbytes)
        out = crypt(ciphertext_v514, newkey)
        niceprint(out)

