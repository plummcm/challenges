
/*
Given an array of integers, return indices of the two numbers such that they add up to a specific target.

You may assume that each input would have exactly one solution, and you may not use the same element twice.

Example:

Given nums = [2, 7, 11, 15], target = 9,

Because nums[0] + nums[1] = 2 + 7 = 9,
return [0, 1].
*/

#include <cassert>
#include <exception>
#include <iostream>
#include <map>
#include <sstream>
#include <vector>

class Solution
{
public:
    std::vector<int> twoSum(std::vector<int>& nums, int target)
    {
        // compliment_map<compliment, index_into_nums>
        std::map<int,int> compliment_map;
        std::map<int,int>::iterator iter;
        int compliment;

        // explicit iterator practice. Should normally use 'auto' when declaring iterators
        for(std::vector<int>::iterator i = nums.begin(); i < nums.end(); i++)
        {
            compliment = target - *i; // iterators can be derefed to get values

            iter = compliment_map.find(compliment);

            // Compliment found AND it isn't the same element, return both indices
            if (iter != compliment_map.end() && iter->second != (int) (i - nums.begin()) )
            {
                return std::vector<int> {iter->second, (int) (i - nums.begin())};
            }

            // add current input value into map
            else
            {
                compliment_map[*i] = (int)(i - nums.begin()); // not portable across std:: datatypes
            }
        }

        throw std::invalid_argument("No combination of elements equals target");
    }
};

void ghetto_validate(std::vector<int> input, std::vector<int> output, int target)
{
    std::stringstream tmp;

    tmp << "Input Vector: [";
    for(std::vector<int>::iterator i = input.begin(); i < input.end(); i++)
    {
        tmp << *i << ", ";
    }
    tmp << "]" << std::endl;
    std::cout << tmp.str();
    tmp.clear();

    std::cout << "Target: " << target << std::endl;
    std::cout << "Output: " << output[0] << ", " << output[1] << std::endl;

    int i0 = input.at(output[0]); // no bounds check with []
    int i1 = input.at(output[1]);
    int sum = i0 + i1;

    std::cout << "Sum: " << i0 << " + " << i1 << " = " << sum << std::endl;
    assert(target ==  sum);


}

int main()
{
    Solution s;

    std::vector<int> input = {2, 7, 11, 15};
    //std::vector<int> input = {2, 8, 11, 15}; // no such combo test
    int target = 7;
    //std::vector<int> input = {4, 9, 11, 15}; //same element test
    //int target = 8; //same element test
    std::vector<int> output;

    __try
    {
        output = s.twoSum(input, target);
    }
    catch (const std::invalid_argument& e)
    {
        std::cout << e.what() << std::endl;
        exit(1);
    }

    ghetto_validate(input, output, target);
}