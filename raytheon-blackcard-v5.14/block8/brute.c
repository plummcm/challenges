#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>
#include <omp.h>


#define N 256
#define key_length_in_bytes 4

unsigned int cnt = 0;

char* ciphertext_v514 = "\xF4\x73\xF1\x30\x24\xB5\x4F\x35\x58\xF1\x62\x48\x55";

void swap(unsigned char *Si, unsigned char *Sj) 
{
    int tmp = *Si;
    *Si = *Sj;
    *Sj = tmp;
}
static inline int key_scheduling_algorithm(unsigned char* S, char* key)
{
    //printf("KSA():&S=%p\t&key=%p\n\n", &S, &key);


    unsigned int j = 0;

	// initialize substitution box such that the index is the value
	for(int i = 0; i < N; i++)
	{
        S[i] = i;
	}

	// identity permutation
	for(int i = 0; i < N; i++)
	{
        //j = (j + S[i] + key[i % key_length_in_bytes]) % N;

        //printf("S[i] :&S=%p\t&key=%p\tj=%d\t%d\n", &S, &key, j, S[i]);

        j = j + S[i];

        unsigned int key_index = i % key_length_in_bytes;
        j = j + key[key_index];

        j = j % N;
    
	    swap(&S[i], &S[j]);
	}

	return 0;
}

static inline int PRGA(unsigned char* S, unsigned char* key)
{
	char* plaintext = ciphertext_v514;
	char ciphertext[128] = { 0 };
	int i = 0;
    int j = 0;

    for(size_t n = 0, len = strlen(plaintext); n < len; n++) 
    {
        i = (i + 1) % N;
        j = (j + S[i]) % N;

        swap(&S[i], &S[j]);
        int rnd = S[(S[i] + S[j]) % N];

        ciphertext[n] = rnd ^ plaintext[n];
    }

    printf("[%02x%02x%02x%02x]{%d}: %s\n", key[0], key[1], key[2], key[3], omp_get_thread_num(), ciphertext);
    //printf("%02hhX", ciphertext[i]);

}


void brute_force()
{
    int i, j, k;

    for(i = 0; i <= 255; i++)
    {
        for(j = 0; j <= 255; j++)
        {
#pragma omp parallel for
            for(k = 0; k <= 255; k++)
            {
                printf("omp_get_num_threads()=%d\n", omp_get_num_threads());
                for(int l = 0; l <= 255; l++)
                {
                    unsigned char key[key_length_in_bytes] = { 0 };
                    key[0] = (char)i;
                    key[1] = (char)j;
                    key[2] = (char)k;
                    key[3] = (char)l;

                    unsigned char S[N] = { 0 };

                    //printf("BF() :&S=%p\t&key=%p\t value=%02x %02x %02x %02x\n", &S, &key, key[0], key[1], key[2], key[3]);

                    key_scheduling_algorithm(S, key);
                    PRGA(S, key);

                    //printf("\n");
                }
            }
        }
    }
}

int main()
{
	

	brute_force();

	return 0;
}